package com.springtut1.test;

public class Person {
	private int id;
	private String name;
	private Address address;

	public void setAddress(Address address) {
		this.address = address;
	}

	private int taxId;
	
	public void setTaxId(int taxId) {
		this.taxId = taxId;
	}

	public Person(){}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Person(int id, String name) {

		this.id = id;
		this.name = name;
	}

	public void speak()
	{
		System.out.println("My butt");
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", address=" + address
				+ ", taxId=" + taxId + "]";
	}




}
